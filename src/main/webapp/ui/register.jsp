<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Page</title>
<link rel="stylesheet" type="text/css"
	href="/webjars/bootstrap/css/bootstrap.min.css" />
<script type="text/javascript" src="/webjars/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="/webjars/bootstrap/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var mobile_check = /^[6-9]+[0-9]{9}$/;
	var apiKey = "125b8448-cb58-11eb-8089-0200cd936042";
	var otpToken = "";
	
	function sendOtp(){
		
		if($("#mobile_no").val().length != 10){
			alert("Invalid Mobile NO")
			return false;
		}
		
		$.get("https://2factor.in/API/V1/"+apiKey+"/SMS/+91"+$("#mobile_no").val()+"/AUTOGEN", function(data, status){
		   //alert("Data: " + data + "\nStatus: " + status);
			if(data.Status == "Success"){
				otpToken = data.Details;
				alert("OTP Send Successfully");
			}
			else{
				alert("Please Resend Otp");
			}
		});
	}
	
	
	function verifyOtp(){
		$.get("https://2factor.in/API/V1/"+apiKey+"/SMS/VERIFY/"+otpToken+"/"+$("#otp").val(), function(data, status){
		    
		    if(data.Status == "Success"){
		    	alert("Otp Match");
		    	if($("#user_id").val().trim() == "" ){
		    		$("#errMsg1").html("Enter User Name").show().fadeOut(8000);
			 	    $("#user_id").focus();
		    		return false;
		    	}
		    	
		    	if($("#password").val().trim() == "" ){
		    		$("#errMsg2").html("Enter Password").show().fadeOut(8000);
			 	    $("#password").focus();
		    		return false;
		    	}
		    	
		    	if($("#name").val().trim() == "" ){
		    		$("#errMsg3").html("Enter Name").show().fadeOut(8000);
			 	    $("#name").focus();
		    		return false;
		    	}
		    	
		    	if($("#mobile_no").val().trim() == "" ){
		    		$("#errMsg4").html("Enter Mobile Number").show().fadeOut(8000);
			 	    $("#mobile_no").focus();
		    		return false;
		    	}
		    	
		    	if($("#otp").val().trim() == "" ){
		    		$("#errMsg5").html("Enter Mobile Number").show().fadeOut(8000);
			 	    $("#otp").focus();
		    		return false;
		    	}
		    	
		    	if($("#date").val().trim() == "" ){
		    		$("#errMsg6").html("Enter Date Of Birth").show().fadeOut(8000);
			 	    $("#date").focus();
		    		return false;
		    	}
		    	
		    	if($("#address").val().trim() == "" ){
		    		$("#errMsg7").html("Enter Address").show().fadeOut(8000);
			 	    $("#address").focus();
		    		return false;
		    	}
		    	
		    	
		    	$("#register").submit();
		    } else{
		    	alert("Otp Not Match.....!");
		    }
		});
		
		
		return false;
	}
	
	$("#sendOtp").click(function(){
		sendOtp();
		
		$("#sendOtp").val("Resend Otp");
	});
	
	$("#singUp").click(function(){
				
		verifyOtp();
	});
	
	$("#mobile_no").keypress(function (e) {
			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		        $("#errmobile").html("digit only").show().fadeOut("slow");
		         return false;
		    }
		   }); 
	
});

</script>	

</head>
<body>
	<div class="container text-center">
		<div>
			<h1>User Registration - Sign Up</h1>
		</div>
	
		<font color="red">${errorMessage}</font>
	
	<div class="container alert " id="new_user" style="width: 70%; border: 3px; background-color: #e2e2e2;">
   	 <form method="post" id="register">
	   
	    
	    <div class="m-3">
				<div class="form-group row">
					<label class="col-4 col-form-label"><strong>User Name :</strong>  </label>
					<div class="col-8">
						<input class="form-control" maxlength="25"
					type="text" id="user_id" name="User_name" required/>
					<span style="margin-left:80px;font-size:14px;color:red" id="errMsg1"></span>
					</div>
				</div>
				
				
		    <div class="form-group row">
					<label class="col-4 col-form-label"><strong>Password :</strong>  </label>
					<div class="col-8">
						<input class="form-control" maxlength="25"
					type="text" id="password" name="password" required/>
					<span style="margin-left:80px;font-size:14px;color:red" id="errMsg2"></span>
					</div>
				</div>
				
				
			 <div class="form-group row">
					<label class="col-4 col-form-label"><strong>Name :</strong>  </label>
					<div class="col-8">
						<input class="form-control" maxlength="25"
					type="text" id="name" name="name" required/>
					<span style="margin-left:80px;font-size:14px;color:red" id="errMsg3"></span>
					</div>
				</div>
			
	        
			 <div class="form-group row">
					<label class="col-4 col-form-label"><strong>Mobile No :</strong>  </label>
					<div class="col-8">
						<input class="form-control" maxlength="10" 
					type="text" id="mobile_no" name="mobile_no" required/>
					<span style="margin-left:80px;font-size:14px;color:red" id="errMsg4"></span>
					<a class="btn btn-danger" href="javascript:void(0)" id="sendOtp">Get OTP</a>
					</div>
				</div>
				
				
			 <div class="form-group row">
					<label class="col-4 col-form-label"><strong>Enter OTP  :</strong>  </label>
					<div class="col-8">
						<input class="form-control" maxlength="10"
					type="text" id="otp" name="otp" required/>
					<span style="margin-left:80px;font-size:14px;color:red" id="errMsg5"></span>
					</div>
				</div>
				
			
			<div class="form-group row">
					<label class="col-4 col-form-label"><strong>Date Of bitrh  :</strong>  </label>
					<div class="col-8">
						<input type="date" name="dob" class="form-control" id="applicant_dob" required/>
						<span style="margin-left:80px;font-size:14px;color:red" id="errMsg6"></span>
					</div>
				</div>
			
			 <div class="form-group row">
					<label class="col-4 col-form-label"><strong>Address  :</strong>  </label>
					<div class="col-8">
						<input class="form-control" maxlength="125"
					type="text" id="address" name="address"  required/>
					<span style="margin-left:80px;font-size:14px;color:red" id="errMsg7"></span>
					</div>
				</div>
				
			<div>
					<button type="button" id="singUp" class="btn btn-primary">Sign Up</button>
				</div>
			
	     </div>
    </form>
    </div></div>
</body>
</html>