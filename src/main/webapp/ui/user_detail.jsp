<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Details</title>
<link rel="stylesheet" type="text/css" href="/webjars/bootstrap/css/bootstrap.min.css" />
</head>
<body>
	<div style="float: right;"><a style="text-decoration: none;" href="/logout?login"><strong>Logout</strong></a></div>
	<div class="container text-center">
		<div>
			<h1>User Details</h1>
		</div>
		<div class="container alert " id="new_user"
			style="width: 200%; border: 3px; background-color: #e2e2e2;">
			<table class="table ">

				<tr>
					<th>Sr.No</th>
					<th>Name</th>
					<th>User Name</th>
					<th>Password</th>
					<th>Date Of Birth</th>
					<th>Mobile No</th>
					<th>OTP</th>
				</tr>
				<%int i= 1; %>
				<c:forEach items="${users}" var="user">
					<tr>
						<td><%= i++ %></td>
						<td>${user.name }</td>
						<td>${user.userName }</td>
						<td>${user.password }</td>
						<td>${user.dob }</td>
						<td>${user.mobileNo }</td>
						<td>${user.otp}</td> 
						<td></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>