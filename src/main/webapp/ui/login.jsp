<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%> 
<!DOCTYPE html>
<html>
<head>
 <meta charset="ISO-8859-1"> 
<title>login page</title>
<link rel="stylesheet" type="text/css"
	href="/webjars/bootstrap/css/bootstrap.min.css" />

</head>
<body>
	<div class="container text-center">

	<h1>Welcome To Login Page</h1>
	<font color="red">${errorMessage} ${msg}</font>
	
	<div class="container alert " id="new_user" style="width: 50%; border: 3px; background-color: #e2e2e2;">
   		 <form method="post">
	    
	     <div class="m-3">
				<div class="form-group row">
					<label class="col-4 col-form-label"><strong>User Name :</strong>  </label>
					<div class="col-8">
						<input class="form-control" maxlength="25"
					type="text" id="name" name="name" required/>
					</div>
				</div>
				
				
		    <div class="form-group row">
					<label class="col-4 col-form-label"><strong>Password :</strong>  </label>
					<div class="col-8">
						<input class="form-control" maxlength="25"
					type="text" id="password" name="password" required/>
					</div>
				</div>
				
				<div>
					<button type="submit" class="form-control btn btn-primary">Login</button>
					&nbsp;&nbsp;<a href="/register"><strong>Sign Up</strong></a>
				</div>
			
	     </div>
    </form>
    </div></div>
</body>
</html>