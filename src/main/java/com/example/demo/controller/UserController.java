package com.example.demo.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.example.demo.repository.UserMasterRepo;

@Controller
public class UserController {

	@Autowired
	private UserMasterRepo userMaster;

	@GetMapping("/users")
	public String userDiaplayPage(ModelMap model, HttpSession session) {
		String username = session.getAttribute("username") + "";

		if (!username.equals("NA")) {
			model.addAttribute("users", userMaster.findByUserName(username));
			return "user_detail";
		} else {
			session.setAttribute("msg", "Login First");
			return "redirect:/login";
		}
	}
}