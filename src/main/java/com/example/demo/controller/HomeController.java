package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.bean.RegistrationBean;
import com.example.demo.service.LoginService;
import com.example.demo.service.RegistrationService;

@Controller
public class HomeController {

	@Autowired
	private LoginService loginService;
	
	@Autowired
	private RegistrationService registrationService;

	@GetMapping("/login")
	public String login(ModelMap model, HttpServletRequest request, HttpSession session) {

		session.setAttribute("username", "NA");
		model.put("errorMessage","");
		session.setAttribute("msg", "");
		return "login";
	}
	
	@GetMapping("/logout")
	public String logout(HttpServletRequest request, HttpSession session) {
		session.setAttribute("username", "NA");
		session.setAttribute("msg", "Logout successful");
		return "login";
	}

	@PostMapping("/login")
	public String showWelcomePage(ModelMap model, @RequestParam String name, @RequestParam String password, HttpSession session) {

		boolean isValidUser = loginService.validateUser(name, password);
		model.put("errorMessage", session.getAttribute("msg"));
		if (!isValidUser) {
			session.setAttribute("username", "NA");
			model.put("errorMessage", "Invalid Credentials");
			return "login";
		}

		model.put("name", name);
		model.put("password", password);
		session.setAttribute("username", name);
		return "redirect:/users";
	}

	@GetMapping("/register")
	public String register(HttpServletRequest request, HttpSession session) {
		return "register";
	}
	String status ="";
	@PostMapping("/register")
	public String postRegister(@ModelAttribute RegistrationBean registrationBean, ModelMap model) {
		status = registrationService.addUserDetails(registrationBean);
		if(status =="ok")
			return "login";
		else {
			model.put("errorMessage","data not saved");
			return "register";
		}
	}

}
