package com.example.demo.dao;

public interface LoginDao {

	public boolean validateUser(String name, String password);

}
