package com.example.demo.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.model.UserMaster;
import com.example.demo.repository.UserMasterRepo;

@Repository
public class LoginDaoImpl implements LoginDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private UserMasterRepo masterRepo;
	
	@Override
	public boolean validateUser(String name, String password) {
		List<UserMaster> user = masterRepo.findByUserNameAndPassword(name, password);
		if(user.size()>0) {
			return true;
		}
		return false;
	}

}
