package com.example.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.bean.RegistrationBean;
import com.example.demo.model.UserMaster;
import com.example.demo.repository.UserMasterRepo;

@Repository
public class RegistrationDaoImp implements RegistrationDao {

	@Autowired
	UserMasterRepo masterRepo;
	
	@Override
	public String addUserDetails(RegistrationBean registrationBean) {
		UserMaster user = new UserMaster();
		try {
			user.setUserName(registrationBean.getUser_name());
			user.setPassword(registrationBean.getPassword());
			user.setName(registrationBean.getName());
			user.setMobileNo(registrationBean.getMobile_no());
			user.setDob(registrationBean.getDob());
			user.setAddress(registrationBean.getAddress());
			user.setOtp(registrationBean.getOtp());
			masterRepo.save(user);
		} catch (Exception e) {
			e.printStackTrace();
			return "not";
		}
		return "ok";
	}

}
