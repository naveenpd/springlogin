package com.example.demo.dao;

import com.example.demo.bean.RegistrationBean;

public interface RegistrationDao {

	String addUserDetails(RegistrationBean registrationBean);

}
