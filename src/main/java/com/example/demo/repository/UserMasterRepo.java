package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.UserMaster;



public interface UserMasterRepo extends JpaRepository<UserMaster, Integer>{

	public List<UserMaster> findByUserNameAndPassword(String name, String password);
	public List<UserMaster> findByUserName(String name);

}
