package com.example.demo.service;

import com.example.demo.bean.RegistrationBean;

public interface RegistrationService {

	String addUserDetails(RegistrationBean registrationBean);

}
