package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.bean.RegistrationBean;
import com.example.demo.dao.RegistrationDao;

@Service
public class RegistrationServiceImp implements RegistrationService {
	@Autowired
	private RegistrationDao registrationDao;
	
	@Override
	public String addUserDetails(RegistrationBean registrationBean) {
		return registrationDao.addUserDetails(registrationBean);
	}

}
